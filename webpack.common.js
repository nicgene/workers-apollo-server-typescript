const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/worker.ts',
  module: {
    rules: [
      {
        test: /\.tsx?$/,
        loader: 'ts-loader',
        options: {
          transpileOnly: true,
        },
      },
    ],
  },
  resolve: {
    mainFields: ['browser', 'main', 'module'],
    extensions: ['.ts', '.tsx', '.js'],
    alias: {
      // Make apollo depends happy
      fs: path.resolve(__dirname, './src/utils/null.ts'),
      net: path.resolve(__dirname, './src/utils/null.ts'),
      tls: path.resolve(__dirname, './src/utils/null.ts')
    }
  },
  target: 'webworker',
  optimization: {
    usedExports: true,
  },
};

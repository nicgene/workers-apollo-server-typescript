const { merge } = require('webpack-merge');
const config = require('./webpack.common.js');
const Dotenv = require('dotenv-webpack');

module.exports = merge(config, {
  mode: 'development',
  devtool: 'source-map', // "cheap-module-source-map"
  plugins: [
    new Dotenv({
      safe: true
    })
  ],
});

# workers-apollo-server-typescript
A boilerplate Apollo GraphQL Server written in TypeScript and ready for deployment to Cloudflare Workers. Implements the Apollo Server `KeyValueCache` interface and is configured to work with Workers KV. This Apollo server also enables CORS on Cloudflare Workers and includes a `RESTDataSource` example. For development only the webpack config included `dotenv-webpack` so you can create a `.env` file to pass environment variables to your Worker.

## Setup
If you haven't already, [install the Workers CLI](https://developers.cloudflare.com/workers/learning/getting-started) and setup your Cloudflare account. Create your own wrangler.toml file from the example included. Add your `account_id`, `zone_id` and create/configure your Workers KV namespaces (`WORKERS_APOLLO_CACHE`).

## Install the dependencies
```bash
yarn install
```

## Run on localhost
```bash
wrangler dev
```
(Running `wrangler dev` without installing dependencies will use npm to install packages.)

## Test example
```bash
curl --location --request POST 'http://127.0.0.1:8787/' \
--header 'Content-Type: application/json' \
--data-raw '{"query":"query pokemon{ pokemon(id: \"1\") { id, name } }","variables":{}}'
```
```bash
# output:
{"data":{"pokemon":{"id":"1","name":"bulbasaur"}}}
```

## Deploy to Cloudflare
Uncomment `env.staging` or `env.production` and setup your [domain DNS record](https://developers.cloudflare.com/workers/platform/routes#subdomains-must-have-a-dns-record) if applicable.
```bash
wrangler publish # Publish the default dev environment to workers_dev
```
```bash
wrangler publish --env staging
```
```bash
wrangler publish --env production
```

## Helpful Wrangler Commands
```bash
# Outputs a list of all KV namespaces associated with your account id.
wrangler kv:namespace list
# Outputs a list of all keys in a given namespace.
wrangler kv:key list --binding=WORKERS_APOLLO_CACHE
wrangler kv:key list --binding=WORKERS_APOLLO_CACHE --env production
wrangler kv:key list --binding=WORKERS_APOLLO_CACHE --prefix="user:1"
```

## Todo
- Upgrade to webpack 5 after wrangler 1.14.x drops

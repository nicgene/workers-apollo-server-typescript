import { ApolloServer, gql } from 'apollo-server-cloudflare';
import { graphqlCloudflare } from 'apollo-server-cloudflare/dist/cloudflareApollo';
import { Request as ApolloRequest, Response } from 'apollo-server-env';
import { PokemonAPI } from '../data/pokeapi';
import { KVCache } from '../utils/cache';
import { CorsConfig } from '../utils/cors';

export interface GraphQLConfig {
  baseEndpoint: string,
  forwardUnmatchedRequestsToOrigin: boolean,
  debug: boolean,
  cors: CorsConfig,
  kvCache: boolean
}

const typeDefs = gql`
  type PokemonSprites {
    front_default: String!
    front_shiny: String!
    front_female: String!
    front_shiny_female: String!
    back_default: String!
    back_shiny: String!
    back_female: String!
    back_shiny_female: String!
  }
  type Pokemon {
    id: ID!
    name: String!
    height: Int!
    weight: Int!
    sprites: PokemonSprites!
  }
  type Query {
    pokemon(id: ID!): Pokemon
  }
`;

const resolvers = {
  Query: {
    pokemon(source: unknown, args: { id: string }, context: { dataSources: { pokemonAPI: PokemonAPI } }) {
      return context.dataSources.pokemonAPI.getPokemon(args.id);
    },
  },
};

const dataSources = () => ({
  pokemonAPI: new PokemonAPI(),
});

const kvCache = { cache: new KVCache() }

const createServer = (graphQLOptions: GraphQLConfig): ApolloServer =>
  new ApolloServer({
    typeDefs,
    resolvers,
    introspection: true,
    dataSources,
    ...(graphQLOptions.kvCache ? kvCache : {}),
  });

export const apollo = (request: Request, graphQLOptions: GraphQLConfig): Promise<Response> => {
  const server = createServer(graphQLOptions);
  return graphqlCloudflare(() => server.createGraphQLServerOptions(request as ApolloRequest))(request as ApolloRequest);
};

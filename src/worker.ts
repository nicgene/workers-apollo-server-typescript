import { apollo, GraphQLConfig } from './handlers/apollo';
import { setCors } from './utils/cors';

const graphQLOptions: GraphQLConfig = {
  baseEndpoint: '/',
  cors: {
    allowOrigin: 'https://www.mydomain.com'
  },
  debug: process.env.NODE_ENV === 'development',
  forwardUnmatchedRequestsToOrigin: false,
  kvCache: true,
};

const handleRequest = async (request: Request): Promise<Response> => {
  const url = new URL(request.url);
  try {
    if (url.pathname === graphQLOptions.baseEndpoint) {
      const response = request.method === 'OPTIONS' ? new Response(null, { status: 204 }) : await apollo(request, graphQLOptions);
      if (graphQLOptions.cors) {
        setCors(response as Response, graphQLOptions.cors);
      }
      return response as Response;
    } else if (graphQLOptions.forwardUnmatchedRequestsToOrigin) {
      return fetch(request);
    } else {
      return new Response('Not found', { status: 404 });
    }
  } catch (error) {
    return new Response(graphQLOptions.debug ? error : 'Something went wrong', { status: 500 });
  }
};

addEventListener('fetch', (event: FetchEvent) => {
  event.respondWith(handleRequest(event.request));
});

export class KVCache {
  async get(key: string) {
    let result = await WORKERS_APOLLO_CACHE.get(key);
    return (result === null) ? undefined : result;
  }

  async set(key: string, value: any, options: any) {
    const opts: any = {}
    const ttl = options && options.ttl
    if (ttl) {
      opts.expirationTtl = ttl
    }
    return await WORKERS_APOLLO_CACHE.put(key, value, opts)
  }

  async delete(key: string) {
    await WORKERS_APOLLO_CACHE.delete(key)
  }
}
